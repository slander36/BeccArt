require 'spec_helper'

describe "guests/show" do
  before(:each) do
    @guest = assign(:guest, stub_model(Guest,
      :name => "Name",
      :email => "Email",
      :website => "Website",
      :twitter => "Twitter",
      :show_name => false,
      :email_updates => false
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Email/)
    rendered.should match(/Website/)
    rendered.should match(/Twitter/)
    rendered.should match(/false/)
    rendered.should match(/false/)
  end
end
