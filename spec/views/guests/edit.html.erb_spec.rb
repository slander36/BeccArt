require 'spec_helper'

describe "guests/edit" do
  before(:each) do
    @guest = assign(:guest, stub_model(Guest,
      :name => "MyString",
      :email => "MyString",
      :website => "MyString",
      :twitter => "MyString",
      :show_name => false,
      :email_updates => false
    ))
  end

  it "renders the edit guest form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => guests_path(@guest), :method => "post" do
      assert_select "input#guest_name", :name => "guest[name]"
      assert_select "input#guest_email", :name => "guest[email]"
      assert_select "input#guest_website", :name => "guest[website]"
      assert_select "input#guest_twitter", :name => "guest[twitter]"
      assert_select "input#guest_show_name", :name => "guest[show_name]"
      assert_select "input#guest_email_updates", :name => "guest[email_updates]"
    end
  end
end
