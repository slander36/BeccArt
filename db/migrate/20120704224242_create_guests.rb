class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.string :name
      t.string :email
      t.string :website
      t.string :twitter
      t.boolean :show_name
      t.boolean :email_updates

      t.timestamps
    end
  end
end
