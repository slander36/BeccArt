class GuestsController < ApplicationController
 
  def index
    @guests = Guest.all
		@guest = Guest.new
  end

	def new
		@guest = Guest.new
	end

	def show
		if can? :manage, Guest
			@guest = Guest.find(params[:id])
		else
			flash[:error] = "You do not have access to that page"
			redirect_to guests_url
		end
	end

  def edit
		if can? :manage, Guest
    	@guest = Guest.find(params[:id])
		else
			flash[:error] = "You do not have access to that page"
			redirect_to guests_url
		end
  end

  def create
		@guests = Guest.all
    @guest = Guest.new(params[:guest])
		if @guest.save
			flash[:success] = "Thank you for signing the Guest Book"
			redirect_to guests_url
		else
			render 'index'
		end
  end

  def update
		if can? :manage, Guest
	    @guest = Guest.find(params[:id])

	    respond_to do |format|
	      if @guest.update_attributes(params[:guest])
	        format.html { redirect_to @guest, notice: 'Guest was successfully updated.' }
	        format.json { head :no_content }
	      else
	        format.html { render action: "edit" }
	        format.json { render json: @guest.errors, status: :unprocessable_entity }
	      end
	    end
		else
			flash[:error] = "You do not have access to that page"
			redirect_to guests_url
		end
  end

  def destroy
		if can? :manage, Guest
	    @guest = Guest.find(params[:id])
	    if @guest.destroy
				flash[:success] = "Guest Signature Removed"
				redirect_to guests_url
			else
				flash.now[:error] = "Failed to Remove Guest Signature"
				render 'index'
			end
		else
			flash[:error] = "You do not have access to that page"
			redirect_to guests_url
		end
  end
end
