class ApplicationController < ActionController::Base
  protect_from_forgery

	# Change current_user to current_owner for rails_admin setup
	def current_ability
		@current_ability ||= Ability.new(current_owner)
	end
end
