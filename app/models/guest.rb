class Guest < ActiveRecord::Base
  attr_accessible :email, :email_updates, :name, :show_name, :twitter, :website

	before_save { |guest| guest.name = name.titleize }
	before_save { |guest| guest.email = email.downcase }

	VALID_NAME_REGEX = /\A[a-zA-Z][a-z]+( [a-zA-Z](\.)?([a-z]+)?)?( [a-zA-Z](\.)?([a-z]+)?)?\z/

	validates :name,
		presence: true,
		format: { with: VALID_NAME_REGEX },
		uniqueness: { case_sensitive: false }

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

	validates :email,
		presence: true,
		format: { with: VALID_EMAIL_REGEX },
		uniqueness: { case_sensitive: false }

	VALID_WEB_REGEX = /\A(www\.)?[\w\-.]+\.[a-z]+([\w\\\-]+)?\z/i

	validates :website,
		allow_blank: true,
		format: { with: VALID_WEB_REGEX }

	validates :twitter,
		allow_blank: true,
		uniqueness: { case_sensitive: true }

end
